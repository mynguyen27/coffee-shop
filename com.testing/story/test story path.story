Meta:
  @test
  
  Scenario:
    <Some interesting scenario steps here>
    
    Narrative:
      Given aeSite header set as <aeSite>
      And user type <userType>
      When I add 'commerce' item 'without promotion' with quantity 1
      Then there are no errors and status is success
  
    Lifecycle:
      When I add shipping address to cart: commerce/data/common/shippingAddress/<shippingAddress>
      Then there are no errors and status is success
    
    Before:
      When I add gift card 'Gift_Card_25_USD_03'
      Then there are no errors and status is success
      
    Scope:
      When I view cart
      Then payment response contains correct recalculated payment amount
      
    After:
      When I add 'commerce' item 'without promotion' with quantity 1
      When I view cart
      Then payment response contains correct recalculated payment amount

